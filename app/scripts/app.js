'use strict';

/**
 * @ngdoc overview
 * @name dichosmixerApp
 * @description
 * # dichosmixerApp
 *
 * Main module of the application.
 */
angular
  .module('dichosmixerApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/dicho', {
        templateUrl: 'views/main.html',
        controller: 'mainController'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'aboutController'
      })
      .otherwise({
        redirectTo: '/dicho'
      });
  });
