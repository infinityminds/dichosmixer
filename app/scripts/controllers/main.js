'use strict';

/**
 * @ngdoc function
 * @name dichosmixerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dichosmixerApp
 */
angular.module('dichosmixerApp')
	.controller('mainController', function ($scope) {
    	var dichosList=[ 
	    	{'inicio':'Quien bien te quiere, ', 'final':'te hará llorar'},
	      	{'inicio':'La memoria es como el mal amigo,','final':'cuando más falta te hace, te falla'},
	    	{'inicio':'Cuando el hombre es celoso, molesta, ', 'final':'cuando no lo es, irrita'},
	      	{'inicio':'Más vale feo y bueno,','final':'que guapo y perverso'},
	    	{'inicio':'La probabilidad de hacer mal se encuentra cien veces al día, ', 'final':'la de hacer bien una vez al año'},
	      	{'inicio':'Cuando fuiste martillo no tuviste clemencia,','final':'ahora que eres yunque, ten paciencia'},
	    	{'inicio':'Quien no buscó amigos en la alegría, ', 'final':'en la desgracia no los pida'},
	      	{'inicio':'Nunca es tarde para bien hacer,','final':'haz hoy lo que no hiciste ayer'},
	    	{'inicio':'A quien Dios no le dio hijos, ', 'final':'el diablo le dio sobrinos'},
	      	{'inicio':'Antes que te cases,','final':'mira lo que haces'},
	    	{'inicio':'Más rápido se coge al mentiroso, ', 'final':'que al cojo'},
	      	{'inicio':'Quien da pan a perro ajeno,','final':'pierde el pan y pierde el perro'},
	    	{'inicio':'Mientras hay vida, ', 'final':'hay esperanza'},
	      	{'inicio':'La conciencia es, a la vez,','final':'testigo, fiscal y juez'},
	    	{'inicio':'Quien todo lo quiere, ', 'final':'todo lo pierde'},
	      	{'inicio':'Quien adelante no mira,','final':'atrás se queda'},
	    	{'inicio':'El que la sigue,', 'final':'la consigue'},
	      	{'inicio':'Más vale malo conocido,','final':'que bueno por conocer'},
	    	{'inicio':'El infierno está lleno de buenas intenciones, ', 'final':'y el cielo de buenas obras'},
	      	{'inicio':'No hay plazo que no llegue,','final':'ni deuda que no se pague'},
	    	{'inicio':'Mas vale pajaro en mano, ', 'final':'que cien volando'},
	      	{'inicio':'Camarón que se duerme,','final':'se lo lleva la corriente'},
	      	{'inicio':'El que madruga,','final':'Dios lo ayuda' }
	    ];

		function getRandom(){
	        return dichosList[Math.floor(Math.random()*dichosList.length)].inicio + 
	        dichosList[Math.floor(Math.random()*dichosList.length)].final ;
	    };

		$scope.getDicho=function(){
			$scope.dichoRandom=getRandom(); 
		};

		$scope.dichoRandom =  getRandom();


	});
