'use strict';

/**
 * @ngdoc function
 * @name dichosmixerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dichosmixerApp
 */
angular.module('dichosmixerApp')
  .controller('AboutController', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
